#!/bin/bash

# Set bash script to exit immediatly if any command fail
set -e

echo "build-keyboard-management-framework.sh"
echo "Proccess "$$

# Constants
eval PROJECT_DIR=$1
echo "Project root directory:${PROJECT_DIR}"

XCPROJECT_NAME="KeyboardManagement"

BUILD_DIR="${PROJECT_DIR}/Build/Products"
echo "Build directory:${BUILD_DIR}"

DERIVED_DATA_DIR="${PROJECT_DIR}/DerivedData"

SCHEME_NAME="KeyboardManagement"

CONFIGURATION_NAME="Release"
IPHONE_SIMULATOR_CONFIGURATION_NAME="${CONFIGURATION_NAME}"
IPHONE_CONFIGURATION_NAME="Release"

PRODUCT_NAME="KeyboardManagement"
CONTENTS_FOLDER_PATH="${PRODUCT_NAME}.framework"

BUILD_OUTPUT_DIR="${PROJECT_DIR}/build-output"
BUILD_OUTPUT_RELEASE_DIR="${BUILD_OUTPUT_DIR}/Release"

echo "build..."
#iphonesimulator
xcodebuild build -project "${PROJECT_DIR}/${XCPROJECT_NAME}.xcodeproj" -scheme "${SCHEME_NAME}" -configuration "${IPHONE_SIMULATOR_CONFIGURATION_NAME}" -arch i386 -arch x86_64 -sdk iphonesimulator OBJROOT="${BUILD_DIR}" SYMROOT="${BUILD_DIR}" -quiet ONLY_ACTIVE_ARCH=NO

#iphoneos
xcodebuild archive -project "${PROJECT_DIR}/${XCPROJECT_NAME}.xcodeproj" -scheme "${SCHEME_NAME}" -configuration "${IPHONE_CONFIGURATION_NAME}" -arch arm64 -arch armv7 -arch armv7s -sdk iphoneos OBJROOT="${BUILD_DIR}" SYMROOT="${BUILD_DIR}" -quiet ONLY_ACTIVE_ARCH=NO


echo "copy..."
ditto "${BUILD_DIR}/${IPHONE_CONFIGURATION_NAME}-iphoneos/${CONTENTS_FOLDER_PATH}" "${BUILD_DIR}/${CONFIGURATION_NAME}-Universal/${CONTENTS_FOLDER_PATH}"

ditto "${BUILD_DIR}/${IPHONE_SIMULATOR_CONFIGURATION_NAME}-iphonesimulator/${CONTENTS_FOLDER_PATH}/Modules/${PRODUCT_NAME}.swiftmodule" "${BUILD_DIR}/${CONFIGURATION_NAME}-Universal/${CONTENTS_FOLDER_PATH}/Modules/${PRODUCT_NAME}.swiftmodule"

echo "lipo..."
lipo -create "${BUILD_DIR}/${IPHONE_CONFIGURATION_NAME}-iphoneos/${CONTENTS_FOLDER_PATH}/${PRODUCT_NAME}" "${BUILD_DIR}/${IPHONE_SIMULATOR_CONFIGURATION_NAME}-iphonesimulator/${CONTENTS_FOLDER_PATH}/${PRODUCT_NAME}" -output "${BUILD_DIR}/${CONFIGURATION_NAME}-Universal/${CONTENTS_FOLDER_PATH}/${PRODUCT_NAME}"

echo "tar..."
tar -czv -C "${BUILD_DIR}/${CONFIGURATION_NAME}-Universal" -f "${BUILD_OUTPUT_RELEASE_DIR}/${PRODUCT_NAME}-Universal-${CONFIGURATION_NAME}.tar.gz" "${CONTENTS_FOLDER_PATH}"

echo "Universal output directory:${BUILD_OUTPUT_RELEASE_DIR}"
echo "build-keyboard-management-framework.sh-successful"

