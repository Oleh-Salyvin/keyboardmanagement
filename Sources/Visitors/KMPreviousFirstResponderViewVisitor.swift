//
//  KMPreviousFirstResponderViewVisitor.swift
//  KeyboardManagement iOS
//
//  Created by Oleh Salyvin on 29.01.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit

protocol KMPreviousFirstResponderViewVisitorProtocol: KMViewVisitorProtocol {}

struct KMPreviousFirstResponderViewVisitor: KMPreviousFirstResponderViewVisitorProtocol
{
    init() {}
    
    private weak var currentFirstResponder: UIView?
    
    //MARK: - KMViewVisitorProtocol
    
    weak var firstResponder: UIView?
    
    mutating func visit(view: UIView, firstResponders: Array<UIView>)
    {
        guard self.firstResponder == nil else { return }
        
        if self.currentFirstResponder == nil
        {
            func currentFirstResponder() -> UIView?
            {
                for element in firstResponders
                {
                    if element.isFirstResponder
                    {
                        return element
                    }
                }
                
                return nil
            }
            
            self.currentFirstResponder = currentFirstResponder()
        }
        
        if self.currentFirstResponder != nil
        {
            func previousFirstResponder() -> UIView?
            {
                for element in firstResponders
                {
                    if element.isFirstResponder
                    {
                        if let _index = firstResponders.index(of: element), 0 <= _index - 1
                        {
                            return firstResponders[_index - 1]
                        }
                        else
                        {
                            return nil
                        }
                    }
                }
                
                return firstResponders.last
            }
            
            self.firstResponder = previousFirstResponder()
        }
    }
}
