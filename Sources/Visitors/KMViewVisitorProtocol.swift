//
//  KMViewVisitorProtocol.swift
//  KeyboardManagement iOS
//
//  Created by Oleh Salyvin on 29.01.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit

public protocol KMViewVisitorProtocol
{
    init()
    
    var firstResponder: UIView? { get }
    
    mutating func visit(view: UIView, firstResponders: Array<UIView>)
}

public protocol KMVisitorComponentViewProtocol
{
    func accept(visitor: inout KMViewVisitorProtocol)
}
