//
//  KMViewVisitor.swift
//  KeyboardManagement iOS
//
//  Created by Oleh Salyvin on 11.11.2017.
//  Copyright © 2017 Oleh Salyvin. All rights reserved.
//

import UIKit

protocol KMNextFirstResponderViewVisitorProtocol: KMViewVisitorProtocol {}

struct KMNextFirstResponderViewVisitor: KMNextFirstResponderViewVisitorProtocol
{
    init() {}
    
    private weak var currentFirstResponder: UIView?
    
    //MARK: - KMViewVisitorProtocol
    
    weak var firstResponder: UIView? = nil
    
    mutating func visit(view: UIView, firstResponders: Array<UIView>)
    {
        guard self.firstResponder == nil else { return }
        
        if self.currentFirstResponder == nil
        {
            func currentFirstResponder() -> UIView?
            {
                for element in firstResponders
                {
                    if element.isFirstResponder
                    {
                        return element
                    }
                }
                
                return nil
            }
            
            self.currentFirstResponder = currentFirstResponder()
        }
        
        if self.currentFirstResponder != nil
        {
            func nextFirstResponder() -> UIView?
            {
                for element in firstResponders
                {
                    if element.isFirstResponder
                    {
                        if let _index = firstResponders.index(of: element), firstResponders.count != _index + 1
                        {
                            return firstResponders[_index + 1]
                        }
                        else
                        {
                            return nil
                        }
                    }
                }
                
                return firstResponders.first
            }
            
            self.firstResponder = nextFirstResponder()
        }
    }
}
