//
//  KeyboardManagement.swift
//  KeyboardManagement iOS
//
//  Created by Oleh Salyvin on 9/17/16.
//  Copyright © 2016 Oleh Salyvin. All rights reserved.
//

import UIKit

public protocol KeyboardManagementDelegate: class
{
    func previousFirstResponder(keyboardManagement: KeyboardManagement) -> UIView?
    
    func nextFirstResponder(keyboardManagement: KeyboardManagement) -> UIView?
}

public extension KeyboardManagementDelegate
{
    func previousFirstResponder(keyboardManagement: KeyboardManagement) -> UIView?
    {
        return nil
    }
    
    func nextFirstResponder(keyboardManagement: KeyboardManagement) -> UIView?
    {
        return nil
    }
}

public protocol KeyboardManagementNotificationDelegate: class
{
    func notification(notification: KMNotification, keyboardManagement: KeyboardManagement)
}

final public class KeyboardManagement
{
    public required init() {}
    
    public weak var scrollView: UIScrollView?
    
    public private(set) weak var firstResponder: UIView?
    
    public weak var delegate: KeyboardManagementDelegate?
    
    public var isEnabled: Bool = false
    
    public weak var notificationDelegate: KeyboardManagementNotificationDelegate?
    
    public var isPipelineEnabled: Bool {
        set {
            self.notificationPipeline.isEnabled = newValue
        }
        get {
            return self.notificationPipeline.isEnabled
        }
    }
    
    private lazy var notificationPipeline = KMNotificationPipeline(keyboardManagement: self)
    
    private lazy var model = KMModel(keyboardManagement: self)
    
    @discardableResult public func previous() -> UIView?
    {
        guard self.firstResponder != nil, self.validate(view: self.firstResponder!) else { return nil }
        
        return self.model.context().previousFirstResponder(keyboardManagement: self)
    }
    
    @discardableResult public func next() -> UIView?
    {
        guard self.firstResponder != nil, self.validate(view: self.firstResponder!) else { return nil }
        
        return self.model.context().nextFirstResponder(keyboardManagement: self)
    }
    
    public func shouldBeginEditing(view: UIView) -> Bool
    {
        guard self.validate(view: view) else { return false }
        
        self.firstResponder = view
        
        /* send keyboardWillShow notification if keyboard not changed */
        self.notificationPipeline.sendKeyboardWillShowNotification()
        
        return true
    }
    
    private func validate(view: UIView) -> Bool
    {
        guard self.isEnabled else { return false }
        guard self.scrollView != nil, self.scrollView!.window != nil, self.scrollView!.window!.isKeyWindow else { return false }
        guard view is UITextInput, view.isDescendant(of: self.scrollView!) else { return false }
        
        return true
    }
}
