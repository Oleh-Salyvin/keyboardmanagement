//
//  KMKeyboardNotificationObserver.swift
//  KeyboardManagement iOS
//
//  Created by Oleh Salyvin on 20.03.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit

protocol KMKeyboardNotificationListenerProtocol: class
{
    func keyboardWillShow(notification: Notification)
    
    func keyboardDidShow(notification: Notification)
    
    func keyboardWillHide(notification: Notification)
    
    func keyboardDidHide(notification: Notification)
}

final class KMKeyboardNotificationObserver
{
    required init(subscriber: KMKeyboardNotificationListenerProtocol)
    {
        self.subscriber = subscriber
    }
    
    unowned let subscriber: KMKeyboardNotificationListenerProtocol
    
    var isEnabled: Bool = false {
        didSet {
            if self.isEnabled
            {
                self.removeObservers()
                
                self.addObservers()
            }
            else
            {
                self.removeObservers()
            }
        }
    }
    
    deinit
    {
        self.removeObservers()
    }
    
    @objc private func notifyKeyboardWillShow(notification: Notification)
    {
        self.subscriber.keyboardWillShow(notification: notification)
    }
    
    @objc private func notifyKeyboardDidShow(notification: Notification)
    {
        self.subscriber.keyboardDidShow(notification: notification)
    }
    
    @objc private func notifyKeyboardWillHide(notification: Notification)
    {
        self.subscriber.keyboardWillHide(notification: notification)
    }
    
    @objc private func notifyKeyboardDidHide(notification: Notification)
    {
        self.subscriber.keyboardDidHide(notification: notification)
    }
    
    private func addObservers()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(notifyKeyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(notifyKeyboardDidShow), name: UIResponder.keyboardDidShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(notifyKeyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(notifyKeyboardDidHide), name: UIResponder.keyboardDidHideNotification, object: nil)
    }
    
    private func removeObservers()
    {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardDidShowNotification, object: nil)
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardDidHideNotification, object: nil)
    }
}
