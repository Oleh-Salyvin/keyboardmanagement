//
//  KMResendNotificationTimerFilter.swift
//  KeyboardManagement
//
//  Created by Oleh Salyvin on 19.09.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import Foundation

final class KMResendNotificationFilter
{
    required init(notificationPipeline: KMNotificationPipeline)
    {
        self.notificationPipeline = notificationPipeline
    }

    unowned let notificationPipeline: KMNotificationPipeline

    private(set) var notificationLast: Notification?

    private var notificationCounter: UInt8 = 0

    private lazy var preventResendWorkItem: DispatchWorkItem = DispatchWorkItem(block: { [weak self] in
        guard let self = self else { return }
        guard self.notificationCounter > 0 else { return }

        self.notificationCounter = 0

        self.notificationPipeline.pipe(notification: self.notificationLast!)
    })

    func resendNotificationLast()
    {
        if self.notificationLast != nil
        {
            self.handle(notification: self.notificationLast!)
        }
    }

    func handle(notification: Notification)
    {
        if self.notificationLast == nil
        {
            self.notificationLast = notification

            self.notificationPipeline.pipe(notification: notification)
        }
        else
        {
            self.notificationLast = notification

            if self.notificationCounter == 0
            {
                self.notificationCounter += 1

                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now(), execute: self.preventResendWorkItem)
            }
        }
    }

    func cancel()
    {
        self.notificationCounter = 0
    }
}
