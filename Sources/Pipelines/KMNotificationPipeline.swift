//
//  KMNotificationPipeline.swift
//  KeyboardManagement
//
//  Created by Oleh Salyvin on 21.07.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit

final class KMNotificationPipeline: KMKeyboardNotificationListenerProtocol
{
    required init(keyboardManagement: KeyboardManagement)
    {
        self.keyboardManagement = keyboardManagement

        self.keyboardNotificationObserver = KMKeyboardNotificationObserver(subscriber: self)
    }

    unowned let keyboardManagement: KeyboardManagement

    var isEnabled: Bool = false {
        didSet {
            self.keyboardNotificationObserver?.isEnabled = self.isEnabled
        }
    }

    private var keyboardNotificationObserver: KMKeyboardNotificationObserver?

    private lazy var localNotificationFilter = KMLocalNotificationFilter(notificationPipeline: self)

    private lazy var keyboardWillShowResendNotificationFilter = KMResendNotificationFilter(notificationPipeline: self)

    //MARK: - KMKeyboardNotificationListenerProtocol

    func keyboardWillShow(notification: Notification)
    {
        self.pipeIsLocal(notification: notification)
    }

    func keyboardDidShow(notification: Notification)
    {
        self.pipeIsLocal(notification: notification)
    }

    func keyboardWillHide(notification: Notification)
    {
        self.pipeIsLocal(notification: notification)
    }

    func keyboardDidHide(notification: Notification)
    {
        self.pipeIsLocal(notification: notification)
    }

    func sendKeyboardWillShowNotification()
    {
        self.keyboardWillShowResendNotificationFilter.resendNotificationLast()
    }

    private func pipeIsLocal(notification: Notification)
    {
        self.localNotificationFilter.handle(notification: notification)
    }

    func pipeFake(notification: Notification)
    {
        if notification.name == UIResponder.keyboardWillShowNotification
        {
            self.keyboardWillShowResendNotificationFilter.handle(notification: notification)
        }
        else
        {
            self.keyboardWillShowResendNotificationFilter.cancel()

            self.pipe(notification: notification)
        }
    }

    func pipe(notification: Notification)
    {
        self.keyboardManagement.notificationDelegate?.notification(notification: KMNotification(notification: notification), keyboardManagement: self.keyboardManagement)
    }
}
