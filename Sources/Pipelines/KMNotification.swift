//
//  KMNotification.swift
//  KeyboardManagementDemo
//
//  Created by Oleh Salyvin on 10/29/18.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit

public struct KMNotification
{
    public init(notification: Notification)
    {
        self.notification = notification
    }

    public let notification: Notification

    public var name: Notification.Name
    {
        return self.notification.name
    }

    public var frameEnd: CGRect?
    {
        return self.notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue as? CGRect
    }

    public var duration: CFTimeInterval?
    {
        return self.notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber as? CFTimeInterval
    }

    public var options: UIView.AnimationOptions?
    {
        if let _options = self.notification.userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber as? UInt
        {
            return UIView.AnimationOptions(rawValue: _options)
        }

        return nil
    }
}
