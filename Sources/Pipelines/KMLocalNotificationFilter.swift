//
//  KMLocalNotificationFilter.swift
//  KeyboardManagement
//
//  Created by Oleh Salyvin on 12/4/18.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit

final class KMLocalNotificationFilter
{
    required init(notificationPipeline: KMNotificationPipeline)
    {
        self.notificationPipeline = notificationPipeline
    }

    unowned let notificationPipeline: KMNotificationPipeline

    func handle(notification: Notification)
    {
        if let _isLocal = notification.userInfo?[UIResponder.keyboardIsLocalUserInfoKey] as? NSNumber as? Bool, _isLocal
        {
            self.notificationPipeline.pipeFake(notification: notification)
        }
    }
}
