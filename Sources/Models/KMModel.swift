//
//  KMModel.swift
//  KeyboardManagement
//
//  Created by Oleh Salyvin on 27.08.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit

final class KMModel
{
    required init(keyboardManagement: KeyboardManagement)
    {
        self.keyboardManagement = keyboardManagement
    }
    
    unowned let keyboardManagement: KeyboardManagement
    
    private var contextDefault: KMContext?
    
    func context() -> KeyboardManagementDelegate
    {
        if self.keyboardManagement.delegate != nil
        {
            self.contextDefault = nil
            
            return self.keyboardManagement.delegate!
        }
        else if self.keyboardManagement.scrollView is UICollectionView, self.contextDefault == nil
        {
            self.contextDefault = KMCollectionViewContext(keyboardManagement: self.keyboardManagement)
        }
        else if self.keyboardManagement.scrollView is UITableView, self.contextDefault == nil
        {
            self.contextDefault = KMTableViewContext(keyboardManagement: self.keyboardManagement)
        }
        else if self.contextDefault == nil
        {
            self.contextDefault = KMScrollViewContext(keyboardManagement: self.keyboardManagement)
        }
        
        return self.contextDefault!
    }
}
