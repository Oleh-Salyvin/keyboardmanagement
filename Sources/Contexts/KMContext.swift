//
//  KMContext.swift
//  KeyboardManagement iOS
//
//  Created by Oleh Salyvin on 06.02.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit

class KMContext: KeyboardManagementDelegate
{
    required init(keyboardManagement: KeyboardManagement)
    {
        self.keyboardManagement = keyboardManagement
    }
    
    unowned let keyboardManagement: KeyboardManagement
    
    //MARK: - KeyboardManagementDelegate
    
    func previousFirstResponder(keyboardManagement: KeyboardManagement) -> UIView?
    {
        if keyboardManagement.scrollView != nil
        {
            var visitor: KMViewVisitorProtocol = KMPreviousFirstResponderViewVisitor()
            
            self.visit(view: keyboardManagement.scrollView!, visitor: &visitor)
            
            return visitor.firstResponder
        }
        
        return nil
    }
    
    func nextFirstResponder(keyboardManagement: KeyboardManagement) -> UIView?
    {
        if keyboardManagement.scrollView != nil
        {
            var visitor: KMViewVisitorProtocol = KMNextFirstResponderViewVisitor()
            
            self.visit(view: keyboardManagement.scrollView!, visitor: &visitor)
            
            return visitor.firstResponder
        }
        
        return nil
    }
    
    func visit(view: UIView, visitor: inout KMViewVisitorProtocol) {}
}
