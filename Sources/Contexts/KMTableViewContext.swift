//
//  KMTableViewContext.swift
//  KeyboardManagement iOS
//
//  Created by Oleh Salyvin on 05.02.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit

final class KMTableViewContext: KMScrollViewContext
{
    override func visit(view: UIView, visitor: inout KMViewVisitorProtocol)
    {
        self.visitRecursively(view: view, visitor: &visitor)
    }
    
    private var tableView: UITableView {
        return self.keyboardManagement.scrollView as! UITableView
    }
    
    private func visitRecursively(view: UIView, visitor: inout KMViewVisitorProtocol)
    {
        var subviews: Array<UIView> = view.subviews
        
        if subviews.first is UITableViewCell
        {
            func sortCellsByIndexPath(_ c0: UITableViewCell, _ c1: UITableViewCell) -> Bool
            {
                let indexPath0: IndexPath! = self.tableView.indexPath(for: c0)
                let indexPath1: IndexPath! = self.tableView.indexPath(for: c1)
                return (indexPath0.compare(indexPath1) == .orderedAscending)
            }
            
            subviews = self.tableView.visibleCells.sorted(by: sortCellsByIndexPath)
        }
        
        if visitor is KMPreviousFirstResponderViewVisitorProtocol
        {
            subviews = subviews.reversed()
        }
        
        for view in subviews
        {
            if let _component = view as? KMVisitorComponentViewProtocol
            {
                _component.accept(visitor: &visitor)
            }
            
            guard visitor.firstResponder == nil else { return }
            
            self.visitRecursively(view: view, visitor: &visitor)
        }
    }
}
