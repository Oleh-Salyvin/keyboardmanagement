//
//  KMScrollViewContext.swift
//  KeyboardManagement iOS
//
//  Created by Oleh Salyvin on 05.02.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit

class KMScrollViewContext: KMContext
{
    override func visit(view: UIView, visitor: inout KMViewVisitorProtocol)
    {
        self.visitRecursively(view: view, visitor: &visitor)
    }
    
    private func visitRecursively(view: UIView, visitor: inout KMViewVisitorProtocol)
    {
        var subviews: Array<UIView> = view.subviews
        
        if visitor is KMPreviousFirstResponderViewVisitorProtocol
        {
            subviews = subviews.reversed()
        }
        
        for view in subviews
        {
            if let _component = view as? KMVisitorComponentViewProtocol
            {
                _component.accept(visitor: &visitor)
            }
            
            guard visitor.firstResponder == nil else { return }
            
            self.visitRecursively(view: view, visitor: &visitor)
        }
    }
}
